# Translation of plasma_applet_org.kde.plasma.printmanager.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2013, 2015.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.printmanager\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-04-28 03:34+0200\n"
"PO-Revision-Date: 2015-03-01 12:45+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: plasmoid/package/contents/config/config.qml:27
msgid "General"
msgstr "Opšte"

#: plasmoid/package/contents/ui/config.qml:47
msgid "All jobs"
msgstr "svi poslovi"

#: plasmoid/package/contents/ui/config.qml:53
msgid "Completed jobs only"
msgstr "samo završeni poslovi"

#: plasmoid/package/contents/ui/config.qml:59
msgid "Active jobs only"
msgstr "samo aktivni poslovi"

#: plasmoid/package/contents/ui/PopupDialog.qml:47
msgid "Search for a printer..."
msgstr "Nađi štampač..."

#: plasmoid/package/contents/ui/PopupDialog.qml:92
#: plasmoid/package/contents/ui/printmanager.qml:60
msgid "No printers have been configured or discovered"
msgstr "Nijedan štampač nije podešen niti otkriven."

#: plasmoid/package/contents/ui/PrinterItem.qml:37
#, fuzzy
#| msgid "No active jobs"
msgid "%1, no active jobs"
msgstr "Nema aktivnih poslova"

#: plasmoid/package/contents/ui/PrinterItem.qml:39
#, fuzzy
#| msgid "No active jobs"
msgid "%1, %2 active job"
msgid_plural "%1, %2 active jobs"
msgstr[0] "Nema aktivnih poslova"
msgstr[1] "Nema aktivnih poslova"
msgstr[2] "Nema aktivnih poslova"
msgstr[3] "Nema aktivnih poslova"

#: plasmoid/package/contents/ui/PrinterItem.qml:43
#, fuzzy
#| msgid "No jobs"
msgid "%1, no jobs"
msgstr "Nema poslova"

#: plasmoid/package/contents/ui/PrinterItem.qml:45
msgid "%1, %2 job"
msgid_plural "%1, %2 jobs"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: plasmoid/package/contents/ui/PrinterItem.qml:52
msgid "Resume printing"
msgstr ""

#: plasmoid/package/contents/ui/PrinterItem.qml:52
msgid "Pause printing"
msgstr ""

#: plasmoid/package/contents/ui/PrinterItem.qml:64
#, fuzzy
#| msgid "Configure printer"
msgid "Configure printer..."
msgstr "Podesi štampač"

#: plasmoid/package/contents/ui/PrinterItem.qml:69
#, fuzzy
#| msgid "Open print queue"
msgid "View print queue"
msgstr "Otvori red za štampanje"

#: plasmoid/package/contents/ui/printmanager.qml:39
msgid "Printers"
msgstr "Štampači"

#: plasmoid/package/contents/ui/printmanager.qml:44
msgid "There is one print job in the queue"
msgid_plural "There are %1 print jobs in the queue"
msgstr[0] "%1 posao u redu za štampanje."
msgstr[1] "%1 posla u redu za štampanje."
msgstr[2] "%1 poslova u redu za štampanje."
msgstr[3] "Jedan posao u redu za štampanje."

#: plasmoid/package/contents/ui/printmanager.qml:53
msgctxt "Printing document name with printer name"
msgid "Printing %1 with %2"
msgstr ""

#: plasmoid/package/contents/ui/printmanager.qml:55
msgctxt "Printing with printer name"
msgid "Printing with %1"
msgstr ""

#: plasmoid/package/contents/ui/printmanager.qml:58
msgid "Print queue is empty"
msgstr "Red za štampanje prazan"

#: plasmoid/package/contents/ui/printmanager.qml:86
msgid "&Configure Printers..."
msgstr "&Podesi štampače..."
